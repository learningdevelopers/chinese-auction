/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CA_BL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Batsheva w
 */
public class Bee_Database {
    
        Connection con=null;
        Statement st=null;
        ResultSet rs =null;
        CallableStatement cs =null;
        
         
        public Bee_Database(String user, String password,String address, String database)
        {
            try {
                con=DriverManager.getConnection("jdbc:mysql://"+ address + "/" + database, user, password);
            } catch (SQLException ex) {
                Logger.getLogger(Bee_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public ResultSet executeQuery(String query)
        {
            try {
                st = con.createStatement();
                rs=st.executeQuery(query);
                
            } catch (SQLException ex) {
                Logger.getLogger(Bee_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rs;
        }
        public int executeUpdate(String query)
        {
            int rowCount=0;
            try {
                st = con.createStatement();
                rowCount=st.executeUpdate(query);
                
            } catch (SQLException ex) {
                Logger.getLogger(Bee_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rowCount;
        }
        
        public ResultSet executeSP(String spName, Bee_DatabaseParameter parameter[])
        {
             
                //!!!!!!!!!be able to modify the question marks get the length of the array and put there to get the amount of question marks.
                //!!!!!!!!!depending upon the in/out you have to get a set it in and a registered out if it is an out for each one.
                //!!!!!!!!!but if boolean or int then it cant be setString.
                //!!!!!!!!!translate the parameter type to the type type. we have to assign it correctly.
                //!!!!!!!!!if out param then assign value move it into the correct element in the param []
                 
            try 
            {
                String questionMark;
                String questionMark2="";
                for(int index=0;index<parameter.length;index++)
                {
                    if(index==0)
                    {
                       questionMark="?"; 
                    }
                   else
                    {
                       questionMark=",?";
                    }
                       questionMark2+=questionMark;
                }
                
                cs = con.prepareCall("{call " + spName + " (" + questionMark2+ ")}");
                
                for (int i = 0; i < parameter.length; i++)
                    {
                   
                        if(parameter[i].IsInParam())
                        {
                            switch (parameter[i].GetType())
                            {
                             case STRING:
                                     cs.setString(i+1, parameter[i].GetValueString());
                                     break;
                             case BOOLEAN:
                                     cs.setBoolean(i+1, parameter[i].GetValueBoolean());
                                     break;
                             case INT:
                                     cs.setInt(i+1, parameter[i].GetValueInt());
                                     break;
                            case DOUBLE:
                                    cs.setDouble(i+1, parameter[i].GetValueDouble());
                                    break;
                            /*case LOCALTIME:
                                  cs.gi+1, parameter[i].GetValueLocalTime());*/
                             
                            }
                        }     
                        else 
                       {
                        int type=0;
                        switch (parameter[i].GetType())
                        {
                        case STRING:
                             type=Types.VARCHAR;
                             break;
                        case BOOLEAN:
                             type=Types.BOOLEAN;
                             break;
                        case INT:
                             type=Types.NUMERIC;
                             break;
                        case DOUBLE:
                             type=Types.DOUBLE;
                             break;
                       /* case LOCALTIME:
                            type=Types.TIMESTAMP;
                            break;*/
                        }
                    cs.registerOutParameter(i+1,type);
                      }
                
                    }  
                
                rs=cs.executeQuery();
                for (int i = 0; i < parameter.length; i++) 
                {
                    if (!parameter[i].IsInParam())
                    {
                         switch (parameter[i].GetType())
                        {
                        case STRING:
                             parameter[i].SetValue(cs.getString(i+1));
                             break;
                        case BOOLEAN:
                             parameter[i].SetBoolean(cs.getBoolean(i+1));
                             break;
                        case INT:
                             parameter[i].SetInt(cs.getInt(i+1));
                             break;
                        case DOUBLE:
                             parameter[i].setDouble(cs.getDouble(i+1));
                             break;
                           /* case LOCALTIME:
                            parameter[i].setLocalTime(LocalTime );
                            break;*/
                        }
                    }
                }
            }   

           catch(SQLException ex)
           {
               
           }
             return rs; 
        }
}


