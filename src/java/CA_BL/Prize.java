/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

/**
 *
 * @author Batsheva w
 */
public class Prize {
   
private String picFileName;
private double price;
private int prizenum;
private String prizename;
private String description;
private int quantity;

public Prize()
{
    picFileName="";
    price=0;
    prizenum=0;
    prizename="";
    description="";
    quantity=0;
}
public Prize(Prize pr)
{
    picFileName=pr.getPicFileName();
    price=pr.getPrice();
    prizenum=pr.getPrizenum();
    prizename=pr.getPrizename();
    description=pr.getDescription();
    quantity=pr.getQuantity();
}
public Prize(String fileName)
{
    picFileName=fileName;
}
public Prize(double pric, int prizenu,String prizenam, String desc)
{
    price=pric;
    prizenum=prizenu;
    prizename=prizenam;
    description=desc;
}
    public String getPicFileName() {
        return picFileName;
    }

    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPrizenum() {
        return prizenum;
    }

    public void setPrizenum(int prizenum) {
        this.prizenum = prizenum;
    }

    public String getPrizename() {
        return prizename;
    }

    public void setPrizename(String prizename) {
        this.prizename = prizename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
