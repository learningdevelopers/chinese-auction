/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CA_BL;

import java.time.LocalTime;


/**
 *
 * @author Batsheva w
 */
public class Bee_DatabaseParameter {
    
    public enum Bee_DatabaseType {STRING,BOOLEAN,INT,LOCALTIME,DOUBLE} 
    private String valueString;
    private boolean in;
    private Bee_DatabaseType type;
    private int valueInt;
    private boolean valueBoolean;
    private LocalTime valuelt;
    private double valuedouble;
    
    public Bee_DatabaseParameter(String val,boolean inOrOut,Bee_DatabaseType typeOfParam)
    {
        valueString=val;
        in=inOrOut;
        type=typeOfParam;
    }
    public Bee_DatabaseParameter(int newInt,boolean inOrOut,Bee_DatabaseType typeOfParam)
    {
        valueInt=newInt;
        in=inOrOut;
        type=typeOfParam;
    }
    public Bee_DatabaseParameter(double newDouble,boolean inOrOut,Bee_DatabaseType typeOfParam)
    {
        valuedouble=newDouble;
        in=inOrOut;
        type=typeOfParam;
    }
    public Bee_DatabaseParameter(LocalTime lt,boolean inOrOut,Bee_DatabaseType typeOfParam)
    {
        valuelt=lt;
        in=inOrOut;
        type=typeOfParam;
    }
    public Bee_DatabaseParameter(boolean newBoolean,boolean inOrOut,Bee_DatabaseType typeOfParam)
    {
        valueBoolean=newBoolean;
        in=inOrOut;
        type=typeOfParam;
    }
    public void SetValue (String newValue)
    {
        valueString=newValue;
    }
    public void SetBoolean (boolean newBoolean)
    {
        valueBoolean=newBoolean;
    }
    public void SetInt(int newInt)
    {
        valueInt=newInt;
    }
    public void SetTypeOfParam (Bee_DatabaseType newType)
    {
        type=newType;
    }
    public boolean IsInParam()
    {
        boolean isIn=false;
        if(in)
        {
            isIn=true;
        }
        return isIn;
    }
    public Bee_DatabaseType GetType()
    {
        return type;
    }
    public String GetValueString ()
    {
        return valueString;
    }
    public boolean GetValueBoolean()
    {
        return valueBoolean;
    }
    public int GetValueInt()
    {
        return valueInt;
    }
    public void setLocalTime(LocalTime lt)
    {
        valuelt=lt;
    }
    public void setDouble(double d)
    {
        valuedouble=d;
    }
    public LocalTime GetValueLocalTime()
    {
        return valuelt;
    }
    public double GetValueDouble()
    {
        return valuedouble;
    }
   /*
1. Make the class variables all 'private'
2. Create public access methods that set the three values (similar to the constructor we created). Create one each for String, Boolean and int,
3. Create the following public access methods
    a. Boolean IsInParam()
    b. Bee_DataBaseParameterType GetType()
    c. String GetValueString()
    d. Boolean GetValueBoolean()
    e. int    GetValueInt()
4. Create two additional constructors
    a. For Int values
    b. For Boolean values

After this is complete modify the application to use these new methods. */
}
