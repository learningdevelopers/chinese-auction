/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

import java.util.ArrayList;

/**
 *
 * @author Batsheva w
 */
public class Person {
  
public ArrayList <Prize> priz=new ArrayList<>();
private String fname;
private String lname;
private String email;
private String phone;
private String address;
private String city;
private String state;
private String zip;
private int pkid;

public Person()
{
    fname="";
    lname="";
    email="";
    phone="";
    address="";
    city="";
    state="";
    zip="";
    pkid=0;
}
public Person(Person per)
{
    fname=per.fname;
    lname=per.lname;
    email=per.email;
    phone=per.phone;
    address=per.address;
    city=per.city;
    state=per.state;
    zip=per.zip;
    pkid=per.getPkid();
}
public Person(Person per,int perpkid)
{
    fname=per.fname;
    lname=per.lname;
    email=per.email;
    phone=per.phone;
    address=per.address;
    city=per.city;
    state=per.state;
    zip=per.zip;
    pkid=perpkid;
}
public Person(String fnam,String lnam, String emai, String phon,
        String addres,String cit, String stat, String zi)
{
    fname=fnam;
    lname=lnam;
    email=emai;
    phone=phon;
    address=addres;
    city=cit;
    state=stat;
    zip=zi;
}

public Person (String fnam,String lnam, String emai, String phon)
{
    fname=fnam;
    lname=lnam;
    email=emai;
    phone=phon;
}

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getFullAddress()
    {
        return address+city+state+zip;
    } 
    public int getPkid() {
        return pkid;
    }

    public void setPkid(int pkid) {
        this.pkid = pkid;
    }
@Override
    public String toString()
    {
        return fname+" "+lname +" "+email+" "+phone;
    }

  
}
