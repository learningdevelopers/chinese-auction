/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

/**
 *
 * @author Batsheva w
 */
public class Billing {
    
    private int ordernum;
    private int fkper;
    private String date;
    private double total;
    private String address;
    private String city;
    private String state;
    private String zip;

    public Billing()
    {
        ordernum=0;
        fkper=0;
        date="";
        total=0;
        address="";
        city="";
        state="";
        zip="";
    }
    public Billing(Billing b)
    {
        ordernum=b.getOrdernum();
        fkper=b.getFkper();
        date=b.getDate();
        total=b.getTotal();
        address=b.address;
        city=b.city;
        state=b.state;
        zip=b.zip;
    }
    public Billing(int ordernu, int fkpe, String dat, double tota, String addres, 
            String cit,String stat, String zi)
    {
        ordernum=ordernu;
        fkper=fkpe;
        date=dat;
        total=tota;
        address=addres;
        city=cit;
        state=stat;
        zip=zi;
    }
    public int getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(int ordernum) {
        this.ordernum = ordernum;
    }

    public int getFkper() {
        return fkper;
    }

    public void setFkper(int fkper) {
        this.fkper = fkper;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
    
}
