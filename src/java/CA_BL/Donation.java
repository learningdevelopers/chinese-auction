/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

/**
 *
 * @author Batsheva w
 */
public class Donation {
    private int pkidper;
    private double donationAmount;

    public Donation()
    {
        pkidper=0;
        donationAmount=0;
    }
    public Donation(int pkidpe,double donatio)
    {
        pkidper=pkidpe;
        donationAmount=donatio;
    }
    public int getPkidper() {
        return pkidper;
    }

    public void setPkidper(int pkidper) {
        this.pkidper = pkidper;
    }

    public double getDonation() {
        return donationAmount;
    }

    public void setDonation(double donation) {
        this.donationAmount = donation;
    }
    
}
