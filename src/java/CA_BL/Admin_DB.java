/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Batsheva w
 */
public class Admin_DB {
    
    private static Bee_Database db;
    
    public  Admin_DB ()
    {
        String url = "localhost:3306";
        String database="edispatch";
        String user="testuser";
        String password="Bat76638";
        db=new Bee_Database(user,password,url,database);
    }
    public static boolean LoginAdmin(String username, String password)
    {
        Bee_DatabaseParameter [] param= new Bee_DatabaseParameter[3];
            param[0]=new Bee_DatabaseParameter(username,true , Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[1]=new Bee_DatabaseParameter(password, true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[2]=new Bee_DatabaseParameter(false, false, Bee_DatabaseParameter.Bee_DatabaseType.BOOLEAN);
            db.executeSP("LoginAdmin",param);
        return  param[2].GetValueBoolean();
    }
   
}
