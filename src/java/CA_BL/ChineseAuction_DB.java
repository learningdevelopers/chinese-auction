/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CA_BL;

/**
 *
 * @author Batsheva w
 */
public class ChineseAuction_DB {
    
    private static Bee_Database db;
    
    public  ChineseAuction_DB ()
    {
        String url = "localhost:3306";
        String database="edispatch";
        String user="testuser";
        String password="Bat76638";
        db=new Bee_Database(user,password,url,database);
    }
    
    public Person InsertPerson(Person p)
    {
         Bee_DatabaseParameter [] param= new Bee_DatabaseParameter[10];
            param[0]=new Bee_DatabaseParameter(p.getFname(),true , Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[1]=new Bee_DatabaseParameter(p.getLname(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[2]=new Bee_DatabaseParameter(p.getAddress(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[3]=new Bee_DatabaseParameter(p.getCity(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[4]=new Bee_DatabaseParameter(p.getState(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[5]=new Bee_DatabaseParameter(p.getZip(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[6]=new Bee_DatabaseParameter(p.getEmail(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[7]=new Bee_DatabaseParameter(p.getPhone(), true, Bee_DatabaseParameter.Bee_DatabaseType.STRING);
            param[8]=new Bee_DatabaseParameter(0, false, Bee_DatabaseParameter.Bee_DatabaseType.INT);
            param[9]=new Bee_DatabaseParameter(false, false, Bee_DatabaseParameter.Bee_DatabaseType.BOOLEAN);
            db.executeSP("InsertPerson",param);
            Person p2=new Person (p,param[8].GetValueInt());
        return p2;
    }
    
}
